<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Awal extends CI_Controller {

	function __construct()
	{
						parent::__construct();		
				$this->load->model('m_data');
                $this->load->helper('url');
	}
	public function home(){
		$this->load->view('awal/bg_home');	
	}
	public function index()
	{
		$this->load->view('awal/bg_index');
	}
	public function show_all()
	{
		$data['daftar_siswa'] = $this->m_data->tampil_data()->result();
		$this->load->view('awal/data',$data);
	}
	public function set_cluster(){
		$data['daftar_siswa'] = $this->m_data->tampil_data()->result();
		$this->load->view('awal/awal_cluster',$data);
	}
}
