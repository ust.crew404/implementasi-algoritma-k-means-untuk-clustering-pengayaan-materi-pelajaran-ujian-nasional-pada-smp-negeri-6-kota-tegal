<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create_data extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
      //  $this->load->helper('file');
    }
    public function index()
    {
        $this->load->view('/awal/upload_data');
    }
    public function data_aksi()
    {
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './assets/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv|xlsx';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
        $this->db->query('truncate table daftar_siswa');
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './assets/'.$media['file_name'];
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database                                
                 $data = array(
                    "no"=> $rowData[0][0],
                    "nama_siswa"=> $rowData[0][1],
                    "bahasa_indonesia"=> $rowData[0][2],
                    "bahasa_inggris"=> $rowData[0][3],
                    "matematika"=> $rowData[0][4],
                    "ipa"=> $rowData[0][5]
                );
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("daftar_siswa",$data);
            //    delete_files($media['file_path']);
                     
            }
        redirect('create_data');
        echo "Data berhasil di Upload!";
    }
}
