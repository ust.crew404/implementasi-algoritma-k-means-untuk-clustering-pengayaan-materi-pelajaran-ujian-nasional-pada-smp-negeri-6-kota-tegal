<!DOCTYPE html>
<html>
<title>Selamat Datang</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {font-family: "Lato", sans-serif}
.mySlides {display: none}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-black w3-card">
    <a class="w3-bar-item w3-button w3-padding-large w3-hide-medium w3-hide-large w3-right" href="javascript:void(0)" onclick="myFunction()" title="Toggle Navigation Menu"><i class="fa fa-bars"></i></a>
    <a href="<?php echo base_url('awal/home');?>" class="w3-bar-item w3-button w3-padding-large">HOME</a>
    <a href="<?php echo base_url();?>" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Tentang K-Means</a>
    <a href="#upload" class="w3-bar-item w3-button w3-padding-large w3-hide-small"><font color="yellow">Upload Data</font></a>
    <a href="<?php echo base_url('awal/show_all')?>" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Tampil Data</a>
    <a href="<?php echo base_url('awal/set_cluster')?>" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Proses Hitung</a>
    <a href="javascript:void(0)" class="w3-padding-large w3-hover-red w3-hide-small w3-right"><i class="fa fa-search"></i></a>
  </div>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-bar-block w3-black w3-hide w3-hide-large w3-hide-medium w3-top" style="margin-top:46px">
  <a href="#band" class="w3-bar-item w3-button w3-padding-large">Pengertian</a>
  <a href="#tour" class="w3-bar-item w3-button w3-padding-large">Upload Data</a>
  <a href="#contact" class="w3-bar-item w3-button w3-padding-large">Proses Hitung</a>
  <!-- <a href="#" class="w3-bar-item w3-button w3-padding-large">MERCH</a> -->
</div>

<!-- Page content -->
<div class="w3-content" style="max-width:2000px;margin-top:46px">

  <!-- Automatic Slideshow Images -->
  <div class="mySlides w3-display-container w3-center">
    <img src="<?php echo base_url('assets/home1.png');?>" style="width:50%">
    <div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
      <!-- <h3>Los Angeles</h3>
      <p><b>We had the best time playing at Venice Beach!</b></p>    -->
    </div>
  </div>
  <div class="mySlides w3-display-container w3-center">
    <img src="<?php echo base_url('assets/home2.png');?>" style="width:50%">
    <div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
     <!--  <h3>New York</h3>
      <p><b>The atmosphere in New York is lorem ipsum.</b></p>     -->
    </div>
  </div>
  <div class="mySlides w3-display-container w3-center">
    <img src="<?php echo base_url('assets/home3.png');?>" style="width:50%">
    <div class="w3-display-bottommiddle w3-container w3-text-white w3-padding-32 w3-hide-small">
      <!-- <h3>Chicago</h3>
      <p><b>Thank you, Chicago - A night we won't forget.</b></p>     -->
    </div>
  </div>
  <!-- The Band Section -->
  <div class="w3-container w3-content w3-center w3-padding-64" style="max-width:800px" id="band">
    <div style="margin-top:20px"></div>
    <h1> Silahkan upload file </h1>
<form action="<?php echo base_url('create_data/data_aksi');?>" method="post" enctype="multipart/form-data">
    <input type="file" name="file"/>
    <input type="submit" value="Upload file"/>
    <p><span>file berupa data Excel ^_^ </span></p>
</form>
<br><br><br>
 <!-- Footer -->
 <!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity w3-light-grey w3-xlarge">
  <p class="w3-medium">Powered by <a href="https://www.facebook.com/asami.ayaka0" target="_blank">Muhamad Hasyim Asyari</a><br>A11.2014.08615</p>
</footer>


<script>
// Automatic Slideshow - change image every 4 seconds
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 4000);    
}

// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}

// When the user clicks anywhere outside of the modal, close it
var modal = document.getElementById('ticketModal');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>
