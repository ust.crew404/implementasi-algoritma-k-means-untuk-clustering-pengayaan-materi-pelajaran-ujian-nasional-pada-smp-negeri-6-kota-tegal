<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>K-Means</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 70px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
		font-size:11px;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	a{
	padding:10px;
	color:#FF3300;
	font-weight:bold;
	border:1px solid #666666;
	background-color:#FFFFCC;
	text-decoration:none;
	}
	* {
    box-sizing: border-box;
}


		/* Create two equal columns that floats next to each other */
		.column {
		    float: left;
		    width: 60%;
		    padding: 0px;
		    height: 300px; /* Should be removed. Only for demonstration */
		}
		.column2 {
		    float: left;
		    width: 5%;
		    padding: 0px;
		    height: 300px; /* Should be removed. Only for demonstration */
		}
		.column3 {
		    float: right;
		    width: 30%;
		    padding: 0px;
		    height: 300px; /* Should be removed. Only for demonstration */
		}

		/* Clear floats after the columns */
		.row:after {
		    content: "";
		    display: table;
		    clear: both;
		}
	</style>
</head>
<body>

<div id="container">
	<br><br><br>
	<center><h1><font size="10px">Data Hasil Iterasi</font></h1></center>

	<div id="body">
	
	<table cellpadding="0" border="1" cellspacing="0" width="100%">
		<tr align="center"><td colspan="5"><p><font size='3px'>HASIL</font></p></td></tr>
		<tr align="center"><td ><p ><font size='3px' >No.</p></td><td width="50%"><p><font size='3px'>SISWA</p></td><td><p><font size='3px'>C1</font></p></td><td><p><font size='3px'>C2</font></p></td><td><p><font size='3px'>C3</font></p></td></tr>
		<?php 
		$q2 = $this->db->query('SELECT iterasi FROM centroid_temp ORDER BY iterasi DESC LIMIT 1');
			foreach($q2->result() as $t)
			{
				$it = $t->iterasi;
			}
		$qs = $this->db->query('SELECT * FROM centroid_temp');
		$total = $qs->num_rows();			
			?>
		
		<?php
			$q3 = $this->db->query('SELECT * FROM hasil_akhir where iterasi = '.$it.'');
			$no = 1;
			foreach($q3->result() as $tq)
			{

			$warna1="";
			$warna2="";
			$warna3="";
			if($tq->c1==1){$warna1='#FFFF00';} else{$warna1='#EAEAEA';}
			if($tq->c2==1){$warna2='#FFFF00';} else{$warna2='#EAEAEA';}
			if($tq->c3==1){$warna3='#FFFF00';} else{$warna3='#EAEAEA';}				
		?>
		<tr align="center"><td align="center"><?php echo $no++ ?></td><td align="left"><?php echo $tq->nama_siswa ?></td><td bgcolor="<?php echo $warna1; ?>" align="center"><?php echo $tq->c1; ?></td><td bgcolor="<?php echo $warna2; ?>" align="center"><?php echo $tq->c2; ?></td><td bgcolor="<?php echo $warna3; ?> " align="center"><?php echo $tq->c3; ?></td></tr>
		<?php
			}
		?>
	</table>
	<?php 
		$ket = $this->db->query('SELECT * FROM ket');
			foreach($ket->result() as $k)
			{
				$k1 = $k->c1;
				$k2 = $k->c2;
				$k3 = $k->c3;
	}?>
	<p>   KETERANGAN :</p>
	<div class="row">
  		<div class="column">
			<table width="100%" border="1" >
				<tr>
					<th>Cluster 1<br>(<?php if($k1 < $k2 && $k1 < $k3){
						echo "Kurang Menguasai";
					}else if($k1 > $k2 && $k1 < $k3){
						echo "Cukup Menguasai";
					}else{
						echo "Sangat Menguasai";
					}?>)</th>
					<th>Cluster 2<br>(<?php if($k2 < $k1 && $k2 < $k3){
						echo "Kurang Menguasai";
					}else if($k2 > $k1 && $k2 < $k3){
						echo "Cukup Menguasai";
					}else{
						echo "Sangat Menguasai";
					}?>)</th>
					<th>Cluster 3<br>(<?php if($k3 < $k2 && $k3 < $k1){
						echo "Kurang Menguasai";
					}else if($k3 > $k2 && $k3 < $k1){
						echo "Cukup Menguasai";
					}else{
						echo "Sangat Menguasai";
					}?>)</th>
				</tr>
				<tr>
					<td><?php 
					$ca=1;
		$c1 = $this->db->query('SELECT * FROM hasil_akhir where c1='.$ca.' and iterasi ='.$it.'');
			foreach($c1->result() as $ca)
			{
				echo $ca->nama_siswa."<br>";
	}?></td>
					<td><?php
					 $ca=1;
		$c2 = $this->db->query('SELECT * FROM hasil_akhir where c2='.$ca.' and iterasi ='.$it.'');
			foreach($c2->result() as $cb)
			{
				echo $cb->nama_siswa."<br>";
	}?></td>
					<td><?php
					 $ca=1;
		$c3 = $this->db->query('SELECT * FROM hasil_akhir where c3='.$ca.' and iterasi ='.$it.'');
			foreach($c3->result() as $cc)
			{
				echo $cc->nama_siswa."<br>";
	}?></td>
				</tr>
				<tr>
					<td><?php
					$ca = 1;
					$total_c1 = $this->db->query('SELECT * FROM hasil_akhir where c1='.$ca.' and iterasi ='.$it.'');
					echo "Jumlah Siswa : ".$total_c1->num_rows();
					$coba = $total_c1->num_rows();
				//	print_r($query->num_rows());
				?></td>
				<td><?php
					$ca = 1;
					$total_c2 = $this->db->query('SELECT * FROM hasil_akhir where c2='.$ca.' and iterasi ='.$it.'');
					echo "Jumlah Siswa : ".$total_c2->num_rows();
					$coba1 = $total_c2->num_rows();
				//	print_r($query->num_rows());
				?></td>
				<td><?php
					$ca = 1;
					$total_c3 = $this->db->query('SELECT * FROM hasil_akhir where c3='.$ca.' and iterasi ='.$it.'');
					echo "Jumlah Siswa : ".$total_c3->num_rows();
					$coba2 = $total_c3->num_rows();
				//	print_r($query->num_rows());
				?></td>
				</tr>
			</table><br>
			<p><font size="3px"><?php 
		$cek = $this->db->query('SELECT * FROM cek');
			foreach($cek->result() as $c)
			{
				echo "BCV = ".$c->bcv."<br>";
				echo "WCV = ".$c->wcv."<br>";
				echo "RATIO = ".$c->ratio."<br>";
			}?></font></p>
		</div>
		<div class="column2" >
  		</div>
		<div class="column3" >
    		<h2><font size="6px"><?php 
    		$total_siswa = $this->db->query('SELECT * FROM hasil_akhir where iterasi ='.$it.'');
					echo "Jumlah Siswa : ".$total_siswa->num_rows();
					$tot = $total_siswa->num_rows();
					?></font></h2><hr>
    		<p><font size="4px"><?php $per_c1 = (($coba / $tot) * 100);
    			echo "Persentase C1 sebanyak = ".$per_c1."%";
    		?></p>

    		<p><?php $per_c2 = (($coba1 / $tot) * 100);
    			echo "Persentase C2 sebanyak = ".$per_c2."%";
    		?></p>
    		<p><?php $per_c3 = (($coba2 / $tot) * 100);
    			echo "Persentase C3 sebanyak = ".$per_c3."%";
    		?></p>
  		</div>
	</div>

	<!-- <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p> -->
</div>
</div>
</body>
</html>