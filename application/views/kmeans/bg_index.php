<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>K-Means</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }
 
	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
		font-size:11px;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	a{
	padding:10px;
	color:#FF3300;
	font-weight:bold;
	border:1px solid #666666;
	background-color:#FFFFCC;
	text-decoration:none;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Data Awal</h1>

	<div id="body">
	<a href="<?php echo base_url(); ?>kmeans/lanjut">Proses Iterasi Selanjutnya</a><br><br>

	<table cellpadding="5" border="1" cellspacing="0" width="100%">
		<tr align="center"><td rowspan="2">Kode</td><td rowspan="2">Nama</td><td rowspan="2">Bahasa Indonesia</td><td rowspan="2">Bahasa Inggris</td><td rowspan="2">Matematika</td><td rowspan="2">IPA</td>
		<td colspan="4">Centroid 1</td><td colspan="4">Centroid 2</td><td colspan="4">Centroid 3</td><td rowspan="2">C1</td><td rowspan="2">C2</td><td rowspan="2">C3</td>
		</tr>
		<tr align="center">
			<?php 
			foreach($siswa1 as $s){?>
		<td><?php $set1a= $s->bahasa_indonesia; echo $set1a; ?></td><td><?php $set1b = $s->bahasa_inggris; echo $set1b; ?></td><td><?php $set1c = $s->matematika; echo $set1c; ?></td>
		<td><?php $set1d = $s->ipa; echo $set1d; ?></td>
		<?php } ?>
		<?php 
			foreach($siswa2 as $n){?>
		<td><?php $set2a= $n->bahasa_indonesia; echo $set2a; ?></td><td><?php $set2b = $n->bahasa_inggris; echo $set2b; ?></td><td><?php $set2c = $n->matematika; echo $set2c; ?></td>
		<td><?php $set2d = $n->ipa; echo $set2d; ?></td>
		<?php } ?>
		<?php 
			foreach($siswa3 as $a){?>
		<td><?php $set3a= $a->bahasa_indonesia; echo $set3a; ?></td><td><?php $set3b = $a->bahasa_inggris; echo $set3b; ?></td><td><?php $set3c = $a->matematika; echo $set3c; ?></td>
		<td><?php $set3d = $a->ipa; echo $set3d; ?></td>
		<?php } ?>
		</tr>
		<?php $a = sqrt(pow(($set1a - $set2a),2)+pow(($set1b - $set2b),2)+pow(($set1c - $set2c),2)+pow(($set1d - $set2d),2));
	  $a1 = sqrt(pow(($set1a - $set3a),2)+pow(($set1b - $set3b),2)+pow(($set1c - $set3c),2)+pow(($set1d - $set3d),2));
	  $a2 = sqrt(pow(($set2a - $set3a),2)+pow(($set2b - $set3b),2)+pow(($set2c - $set3c),2)+pow(($set2d - $set3d),2));
	  $bcv1 = $a + $a1 + $a2;
	  echo "BCV = ".$bcv1;
	  $total_set1a = $set1a + $set1b + $set1c + $set1d;
	  $total_set2a = $set2a + $set2b + $set2c + $set2d;
	  $total_set3a = $set3a + $set3b + $set3c + $set3d;

	  ?>
		<?php 
		$c1a = $set1a;
		$c1b = $set1b;
		$c1c = $set1c;
		$c1d = $set1d;
		
		$c2a = $set2a;
		$c2b = $set2b;
		$c2c = $set2c;
		$c2d = $set2d;

		$c3a = $set3a;
		$c3b = $set3b;
		$c3c = $set3c;
		$c3d = $set3d;
		
		$c1a_b = "";
		$c1b_b = "";
		$c1c_b = "";
		$c1d_b = "";

		$c2a_b = "";
		$c2b_b = "";
		$c2c_b = "";
		$c2d_b = "";

		$c3a_b = "";
		$c3b_b = "";
		$c3c_b = "";
		$c3d_b = "";
		
		$hc1=0;
		$hc2=0;
		$hc3=0;
		$hc4=0;
		
		$no=0;
		$arr_c1 = array();
		$arr_c2 = array();
		$arr_c3 = array();
		$arr_c4 = array();
		
		$arr_c1_temp = array();
		$arr_c2_temp = array();
		$arr_c3_temp = array();
		$arr_c4_temp = array();
		
		$this->db->query('truncate table centroid_temp');
		$this->db->query('truncate table hasil_centroid');
		$this->db->query('truncate table wvc');
		$this->db->query('truncate table hasil_akhir');
		$this->db->query('truncate table ket');
		$this->db->query('truncate table cek');

		foreach($siswa->result_array() as $s){  ?>
		<tr><td><?php echo $s['no']; ?></td><td><?php echo $s['nama_siswa']; ?></td><td><?php echo $s['bahasa_indonesia']; ?></td><td><?php echo $s['bahasa_inggris']; ?></td><td><?php echo $s['matematika']; ?></td><td><?php echo $s['ipa']; ?></td>
		
		<td colspan="4"><?php 
			$hc1 = sqrt(pow(($s['bahasa_indonesia']-$c1a),2)+pow(($s['bahasa_inggris']-$c1b),2)+pow(($s['matematika']-$c1c),2)+pow(($s['ipa']-$c1d),2));
			echo $hc1;
		?></td>
		<td colspan="4"><?php 
			$hc2 = sqrt(pow(($s['bahasa_indonesia']-$c2a),2)+pow(($s['bahasa_inggris']-$c2b),2)+pow(($s['matematika']-$c2c),2)+pow(($s['ipa']-$c2d),2));
			echo $hc2;
		?></td>
		<td colspan="4"><?php 
			$hc3 = sqrt(pow(($s['bahasa_indonesia']-$c3a),2)+pow(($s['bahasa_inggris']-$c3b),2)+pow(($s['matematika']-$c3c),2)+pow(($s['ipa']-$c3d),2));
			echo $hc3;
		?></td>
		<?php 
		$ket = "insert into ket(c1,c2,c3) values('".$total_set1a."','".$total_set2a."','".$total_set3a."')";
		$this->db->query($ket);
		$total_hc1 = 0;
		$total_hc2 = 0;	
		$total_hc3 = 0;	

		if($hc1<=$hc2)
		{
			if($hc1<=$hc3)
			{
				$arr_c1[$no] = 1;
				$total_hc1 = pow($hc1, 2);
				$q = "insert into wvc(no,get_nilai) values('','".$total_hc1."')";
		//		print_r($q);
				$this->db->query($q); 
			}
			else
			{
				$arr_c1[$no] = '0';
			}
		}
		else
		{
			$arr_c1[$no] = '0';
		}
		
		if($hc2<=$hc1)
		{
			if($hc2<=$hc3)
			{
				$arr_c2[$no] = 1;
				$total_hc2 = pow($hc2, 2);
				$q = "insert into wvc(no,get_nilai) values('','".$total_hc2."')";
		//		print_r($q);
				$this->db->query($q); 
			}
			else
			{
				$arr_c2[$no] = '0';
			}
		}
		else
		{
			$arr_c2[$no] = '0';
		}
		
		if($hc3<=$hc1)
		{
			if($hc3<=$hc2)
			{
				$arr_c3[$no] = 1;
				$total_hc3 = pow($hc3, 2);
				$q = "insert into wvc(no,get_nilai) values('','".$total_hc3."')";
		//		print_r($q);
				$this->db->query($q); 
			}
			else
			{
				$arr_c3[$no] = '0';
			}
		}
		else
		{
			$arr_c3[$no] = '0';
		}

		if($hc4<=$hc1)
		{
			if($hc4<=$hc2)
			{
				$arr_c4[$no] = 1;
			}
			else
			{
				$arr_c4[$no] = '0';
			}
		}
		else
		{
			$arr_c4[$no] = '0';
		}
		
		
		$arr_c1_temp[$no] = $s['bahasa_indonesia'];
		$arr_c2_temp[$no] = $s['bahasa_inggris'];
		$arr_c3_temp[$no] = $s['matematika'];
		$arr_c4_temp[$no] = $s['ipa'];
		
		$warna1="";
		$warna2="";
		$warna3="";
		?>
		<?php if($arr_c1[$no]==1){$warna1='#FFFF00';} else{$warna1='#ccc';} ?><td bgcolor="<?php echo $warna1; ?>"><?php echo $arr_c1[$no] ;?></td>
		<?php if($arr_c2[$no]==1){$warna2='#FFFF00';} else{$warna2='#ccc';} ?><td bgcolor="<?php echo $warna2; ?>"><?php echo $arr_c2[$no] ;?></td>
		<?php if($arr_c3[$no]==1){$warna3='#FFFF00';} else{$warna3='#ccc';} ?><td bgcolor="<?php echo $warna3; ?>"><?php echo $arr_c3[$no] ;?></td>
		</tr>
		<?php
		
		$q = "insert into centroid_temp(no,iterasi,c1,c2,c3) values('".$s['no']."',1,'".$arr_c1[$no]."','".$arr_c2[$no]."','".$arr_c3[$no]."')";
		$this->db->query($q);
		$hasil_akhir = "insert into hasil_akhir(iterasi,nama_siswa,c1,c2,c3) values(1,'".$s['nama_siswa']."','".$arr_c1[$no]."','".$arr_c2[$no]."','".$arr_c3[$no]."')";
		$this->db->query($hasil_akhir);
		$no++; }

		 $this->db->select('SUM(get_nilai) as total_nilai');
		$this->db->from('wvc');
		$wvc_total = $this->db->get()->row()->total_nilai;
		echo "<br>WCV = ".$wvc_total;
		
		$ratio = ($bcv1 / $wvc_total);
		echo "<br>Ratio = ".$ratio; 
		$cek_akhir = "insert into cek(bcv,wcv,ratio) values('".$bcv1."','".$wvc_total."','".$ratio."')";
		$this->db->query($cek_akhir);
		//centroid baru 1.a
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c1);$i++)
		{
			$arr[$i] = $arr_c1_temp[$i]*$arr_c1[$i];
			if($arr_c1[$i]==1)
			{
				$jum++;
			}
		}
		$c1a_b = array_sum($arr)/$jum;
		
		//centroid baru 1.b
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c2);$i++)
		{
			$arr[$i] = $arr_c2_temp[$i]*$arr_c1[$i];
			if($arr_c1[$i]==1)
			{
				$jum++;
			}
		}
		$c1b_b = array_sum($arr)/$jum;
		
		//centroid baru 1.c
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c3);$i++)
		{
			$arr[$i] = $arr_c3_temp[$i]*$arr_c1[$i];
			if($arr_c1[$i]==1)
			{
				$jum++;
			}
		}
		$c1c_b = array_sum($arr)/$jum;
		
		//centroid baru 1.d
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c1);$i++)
		{
			$arr[$i] = $arr_c4_temp[$i]*$arr_c1[$i];
			if($arr_c1[$i]==1)
			{
				$jum++;
			}
		}
		$c1d_b = array_sum($arr)/$jum;
		
	//	print_r($c1d_b);
		
		//centroid baru 2.a
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c1);$i++)
		{
			$arr[$i] = $arr_c1_temp[$i]*$arr_c2[$i];
			if($arr_c2[$i]==1)
			{
				$jum++;
			}
		}
		$c2a_b = array_sum($arr)/$jum;
		
		//centroid baru 2.b
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c2);$i++)
		{
			$arr[$i] = $arr_c2_temp[$i]*$arr_c2[$i];
			if($arr_c2[$i]==1)
			{
				$jum++;
			}
		}
		$c2b_b = array_sum($arr)/$jum;
		
		//centroid baru 2.c
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c3);$i++)
		{
			$arr[$i] = $arr_c3_temp[$i]*$arr_c2[$i];
			if($arr_c2[$i]==1)
			{
				$jum++;
			}
		}
		$c2c_b = array_sum($arr)/$jum;
		
		//centroid baru 2.d
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c2);$i++)
		{
			$arr[$i] = $arr_c4_temp[$i]*$arr_c2[$i];
			if($arr_c2[$i]==1)
			{
				$jum++;
			}
		}
		$c2d_b = array_sum($arr)/$jum;
		
		
		
		//centroid baru 3.a
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c1);$i++)
		{
			$arr[$i] = $arr_c1_temp[$i]*$arr_c3[$i];
			if($arr_c3[$i]==1)
			{
				$jum++;
			}
		}
		$c3a_b = array_sum($arr)/$jum;
		
		//centroid baru 3.b
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c2);$i++)
		{
			$arr[$i] = $arr_c2_temp[$i]*$arr_c3[$i];
			if($arr_c3[$i]==1)
			{
				$jum++;
			}
		}
		$c3b_b = array_sum($arr)/$jum;
		
		//centroid baru 3.c
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c3);$i++)
		{
			$arr[$i] = $arr_c3_temp[$i]*$arr_c3[$i];
			if($arr_c3[$i]==1)
			{
				$jum++;
			}
		}
		$c3c_b = array_sum($arr)/$jum;
		
		//centroid baru 3.d
		$jum = 0;
		$arr = array();
		for($i=0;$i<count($arr_c3);$i++)
		{
			$arr[$i] = $arr_c4_temp[$i]*$arr_c3[$i];
			if($arr_c3[$i]==1)
			{
				$jum++;
			}
		}
		$c3d_b = array_sum($arr)/$jum;
		
		$q = "insert into hasil_centroid(c1a,c1b,c1c,c1d,c2a,c2b,c2c,c2d,c3a,c3b,c3c,c3d) values('".$c1a_b."','".$c1b_b."','".$c1c_b."','".$c1d_b."','".$c2a_b."','".$c2b_b."','".$c2c_b."','".$c2d_b."','".$c3a_b."','".
		$c3b_b."','".$c3c_b."','".$c3d_b."')";
	//	print_r($q);
		$this->db->query($q);
		
		
		
		?>
	</table>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

</body>
</html>