<!DOCTYPE html>
<html>
<title>HASIL</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {font-family: "Lato", sans-serif}
.mySlides {display: none}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-black w3-card">
    <a href="<?php echo base_url('kmeans/cetak_hasil');?>" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Cetak Hasil</a>
    <a href="<?php echo base_url('awal/set_cluster');?>" class="w3-bar-item w3-button w3-padding-large w3-hide-small">Cek Lagi</a>
  </div>
</div>
