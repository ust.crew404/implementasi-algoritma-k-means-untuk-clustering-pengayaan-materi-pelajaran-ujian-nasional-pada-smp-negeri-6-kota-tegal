-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 25 Feb 2018 pada 08.48
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ku`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cek`
--

CREATE TABLE `cek` (
  `bcv` varchar(6) NOT NULL,
  `wcv` varchar(6) NOT NULL,
  `ratio` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cek`
--

INSERT INTO `cek` (`bcv`, `wcv`, `ratio`) VALUES
('16.668', '60.640', '0.2748');

-- --------------------------------------------------------

--
-- Struktur dari tabel `centroid_temp`
--

CREATE TABLE `centroid_temp` (
  `no` int(11) NOT NULL,
  `iterasi` varchar(11) NOT NULL,
  `c1` varchar(50) NOT NULL,
  `c2` varchar(50) NOT NULL,
  `c3` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `centroid_temp`
--

INSERT INTO `centroid_temp` (`no`, `iterasi`, `c1`, `c2`, `c3`) VALUES
(1, '1', '1', '0', '0'),
(2, '1', '1', '0', '0'),
(3, '1', '0', '1', '0'),
(4, '1', '0', '0', '1'),
(5, '1', '0', '1', '0'),
(6, '1', '0', '0', '1'),
(7, '1', '0', '0', '1'),
(8, '1', '1', '0', '0'),
(0, '2', '1', '0', '0'),
(0, '2', '1', '0', '0'),
(0, '2', '0', '1', '0'),
(0, '2', '0', '0', '1'),
(0, '2', '0', '1', '0'),
(0, '2', '0', '0', '1'),
(0, '2', '0', '0', '1'),
(0, '2', '1', '0', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_siswa`
--

CREATE TABLE `daftar_siswa` (
  `no` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `bahasa_indonesia` varchar(4) NOT NULL,
  `bahasa_inggris` varchar(4) NOT NULL,
  `matematika` varchar(4) NOT NULL,
  `ipa` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daftar_siswa`
--

INSERT INTO `daftar_siswa` (`no`, `nama_siswa`, `bahasa_indonesia`, `bahasa_inggris`, `matematika`, `ipa`) VALUES
(1, 'ABDULLAH ZEIN PRATAMA', '77', '75.8', '75.6', '74.8'),
(2, 'ADI WIJAYA', '77.4', '76', '75', '76.4'),
(3, 'AKHMAD SARIFFUDIN', '77.4', '76.2', '76.2', '78.2'),
(4, 'ALWI ABDUL AZIZ', '82.8', '84', '83.4', '80.4'),
(5, 'DARNINGSIH', '78.4', '75.6', '76.6', '76.8'),
(6, 'DEWI WIDYA SAFITRI', '79.8', '79', '76.4', '77.4'),
(7, 'DICKY BACHTIAR', '80.2', '78.8', '77.4', '78.2'),
(8, 'FARHAN NUR RISKI', '77.6', '76.2', '75.2', '74.4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil`
--

CREATE TABLE `hasil` (
  `kode` varchar(10) NOT NULL,
  `predikat` varchar(30) NOT NULL,
  `d1` int(11) NOT NULL,
  `d2` int(11) NOT NULL,
  `d3` int(11) NOT NULL,
  `d4` int(11) NOT NULL,
  `d5` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_akhir`
--

CREATE TABLE `hasil_akhir` (
  `iterasi` int(11) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `c1` varchar(12) NOT NULL,
  `c2` varchar(12) NOT NULL,
  `c3` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_akhir`
--

INSERT INTO `hasil_akhir` (`iterasi`, `nama_siswa`, `c1`, `c2`, `c3`) VALUES
(1, 'ABDULLAH ZEIN PRATAMA', '1', '0', '0'),
(1, 'ADI WIJAYA', '1', '0', '0'),
(1, 'AKHMAD SARIFFUDIN', '0', '1', '0'),
(1, 'ALWI ABDUL AZIZ', '0', '0', '1'),
(1, 'DARNINGSIH', '0', '1', '0'),
(1, 'DEWI WIDYA SAFITRI', '0', '0', '1'),
(1, 'DICKY BACHTIAR', '0', '0', '1'),
(1, 'FARHAN NUR RISKI', '1', '0', '0'),
(2, 'ABDULLAH ZEIN PRATAMA', '1', '0', '0'),
(2, 'ADI WIJAYA', '1', '0', '0'),
(2, 'AKHMAD SARIFFUDIN', '0', '1', '0'),
(2, 'ALWI ABDUL AZIZ', '0', '0', '1'),
(2, 'DARNINGSIH', '0', '1', '0'),
(2, 'DEWI WIDYA SAFITRI', '0', '0', '1'),
(2, 'DICKY BACHTIAR', '0', '0', '1'),
(2, 'FARHAN NUR RISKI', '1', '0', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_centroid`
--

CREATE TABLE `hasil_centroid` (
  `nomor` int(2) NOT NULL,
  `c1a` varchar(4) NOT NULL,
  `c1b` varchar(4) NOT NULL,
  `c1c` varchar(4) NOT NULL,
  `c1d` varchar(4) NOT NULL,
  `c2a` varchar(4) NOT NULL,
  `c2b` varchar(4) NOT NULL,
  `c2c` varchar(4) NOT NULL,
  `c2d` varchar(4) NOT NULL,
  `c3a` varchar(4) NOT NULL,
  `c3b` varchar(4) NOT NULL,
  `c3c` varchar(4) NOT NULL,
  `c3d` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `hasil_centroid`
--

INSERT INTO `hasil_centroid` (`nomor`, `c1a`, `c1b`, `c1c`, `c1d`, `c2a`, `c2b`, `c2c`, `c2d`, `c3a`, `c3b`, `c3c`, `c3d`) VALUES
(1, '77.3', '76', '75.2', '75.2', '77.9', '75.9', '76.4', '77.5', '80.9', '80.6', '79.0', '78.6'),
(2, '77.3', '76', '75.2', '75.2', '77.9', '75.9', '76.4', '77.5', '80.9', '80.6', '79.0', '78.6');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ket`
--

CREATE TABLE `ket` (
  `c1` varchar(15) NOT NULL,
  `c2` varchar(15) NOT NULL,
  `c3` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ket`
--

INSERT INTO `ket` (`c1`, `c2`, `c3`) VALUES
('303.7', '307.7', '319.1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rata_rata`
--

CREATE TABLE `rata_rata` (
  `kode` varchar(11) NOT NULL,
  `rata_rata` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `wvc`
--

CREATE TABLE `wvc` (
  `no` int(11) NOT NULL,
  `get_nilai` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `wvc`
--

INSERT INTO `wvc` (`no`, `get_nilai`) VALUES
(1, '0.45'),
(2, '1.49'),
(3, '0.87'),
(4, '37.77'),
(5, '0.87000000000001'),
(6, '11.97'),
(7, '6.45'),
(8, '0.77');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar_siswa`
--
ALTER TABLE `daftar_siswa`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `hasil_centroid`
--
ALTER TABLE `hasil_centroid`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `wvc`
--
ALTER TABLE `wvc`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hasil_centroid`
--
ALTER TABLE `hasil_centroid`
  MODIFY `nomor` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wvc`
--
ALTER TABLE `wvc`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
